use cairo::SvgUnit;
use failure::Error;
use gdk_pixbuf::Pixbuf;
use gio::prelude::*;
use gtk::prelude::*;
use librsvg::CairoRenderer;
use librsvg::SvgHandle;
use libxml::parser::Parser;
use std::io::Read;
use std::path::PathBuf;
use std::str::FromStr;

pub fn render_symbolic(icon_path: &PathBuf, icon_size: i32, is_dark: bool) -> Option<Pixbuf> {
    let gicon = gio::FileIcon::new(&gio::File::new_for_path(&icon_path));
    let theme = gtk::IconTheme::get_default()?;

    let mut flags = gtk::IconLookupFlags::FORCE_SVG;
    flags.insert(gtk::IconLookupFlags::FORCE_SYMBOLIC);
    flags.insert(gtk::IconLookupFlags::FORCE_SIZE);

    let icon_info = theme.lookup_by_gicon(&gicon, icon_size, flags)?;

    let mut fg_color = String::new();
    let warning_color = String::from("#f57900");
    let error_color = String::from("#ff8080");
    let mut success_color = String::new();

    if is_dark {
        fg_color.push_str("#eeeeec");
        success_color.push_str("#33d17a");
    } else {
        fg_color.push_str("#2e3436");
        success_color.push_str("#229656");
    }

    if let Ok((pixbuf, _)) = icon_info.load_symbolic(
        &gdk::RGBA::from_str(&fg_color).unwrap(), // We are sure that RGBA can parse those colors
        Some(&gdk::RGBA::from_str(&success_color).unwrap()),
        Some(&gdk::RGBA::from_str(&warning_color).unwrap()),
        Some(&gdk::RGBA::from_str(&error_color).unwrap()),
    ) {
        return Some(pixbuf);
    }
    None
}

pub fn render(
    handle: &SvgHandle,
    group: String,
    rect: String,
    into: &PathBuf,
) -> Result<(), Error> {
    let renderer = CairoRenderer::new(handle);

    let group = format!("#{}", group);
    let rect = format!("#{}", rect);

    let viewport = {
        let doc = renderer.intrinsic_dimensions();

        cairo::Rectangle {
            x: 0.0,
            y: 0.0,
            width: doc.width.unwrap().length,
            height: doc.height.unwrap().length,
        }
    };

    let (rect, _) = renderer.geometry_for_layer(Some(&rect), &viewport).unwrap();

    let mut surface = cairo::SvgSurface::new(rect.width, rect.height, into);
    surface.set_document_unit(SvgUnit::Px);

    let cr = cairo::Context::new(&surface);
    cr.translate(-rect.x, -rect.y);

    renderer.render_layer(&cr, Some(&group), &viewport)?;
    /*
    if !self.classes.is_empty() {
        let mut src = File::open(&cache)?;
        let mut data = String::new();
        src.read_to_string(&mut data)?;
        for class in self.classes.iter() {
            data = data.replace(&class.1, &format!("{}\" class=\"{}", class.1, class.0));
         }
        src.write(data.as_bytes())?;
    }*/
    Ok(())
}

pub fn get_element(path: &PathBuf, id: String) -> Result<String, Error> {
    let parser = Parser::default();
    let mut file = std::fs::File::open(path)?;
    let mut content = Vec::new();
    file.read_to_end(&mut content)?;
    let document = parser.parse_string(&content).unwrap();

    if let Some(root) = document.get_root_element() {
        let elements = root.findnodes("//*[@id]").unwrap();
        for element in elements.iter() {
            if element.get_attribute("id").unwrap_or("".to_string()) == id {
                return Ok(document.node_to_string(&element));
            }
        }
    }

    failure::bail!("Failed to get some valid element?")
}
