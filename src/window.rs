use failure::Error;
use gio::prelude::*;
use gtk::prelude::*;
use std::cell::RefCell;
use std::io::Read;
use std::rc::Rc;

use crate::rendering;

pub struct Window {
    pub widget: gtk::ApplicationWindow,
    builder: gtk::Builder,
    open_file: Rc<RefCell<Option<gio::File>>>,
}

impl Window {
    pub fn new() -> Rc<Self> {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/symbolic/window.ui");
        let widget: gtk::ApplicationWindow = builder
            .get_object("window")
            .expect("Failed to find the window object");

        let window = Rc::new(Self {
            widget,
            builder,
            open_file: Rc::new(RefCell::new(None)),
        });
        window.init(window.clone());
        window
    }

    fn rerender(&self) -> Result<(), Error> {
        let icon_id_entry: gtk::Entry = self
            .builder
            .get_object("icon_id_entry")
            .expect("Failed to get icon_id_entry");
        let rect_id_entry: gtk::Entry = self
            .builder
            .get_object("rect_id_entry")
            .expect("Failed to get rect_id_entry");
        let symbolic_img: gtk::Image = self
            .builder
            .get_object("symbolic_img")
            .expect("Failed to get symbolic_img");
        let prerendered_buffer: gtk::TextBuffer = self
            .builder
            .get_object("prerendered_buffer")
            .expect("Failed to get prerendered_buffer");
        let rendered_buffer: gtk::TextBuffer = self
            .builder
            .get_object("rendered_buffer")
            .expect("Failed to get rendered_buffer");

        let current_id = icon_id_entry.get_text().unwrap();
        let rect_id = rect_id_entry.get_text().unwrap();

        if let Some(file) = self.open_file.borrow().clone() {
            let mut output_file = glib::get_user_cache_dir().unwrap_or(std::env::temp_dir());
            let mut output_name = file.get_basename().unwrap().to_str().unwrap().to_string();

            let output_name = format!("{}-symbolic.svg", output_name.trim_end_matches(".svg"));
            output_file.push(output_name);

            let input_path = file
                .get_path()
                .ok_or_else(|| failure::err_msg("Failed to get the path"))?;

            let handle = librsvg::Loader::new().read_path(&input_path)?;

            rendering::render(
                &handle,
                current_id.to_string(),
                rect_id.to_string(),
                &output_file,
            )?;

            let mut f = std::fs::File::open(&output_file)?;
            let mut content: String = String::new();
            f.read_to_string(&mut content)?;

            rendered_buffer.set_text(&content);

            let prerendered_content = rendering::get_element(&input_path, current_id.to_string())?;
            prerendered_buffer.set_text(&prerendered_content);

            let pixbuf = rendering::render_symbolic(&output_file, 128, false)
                .expect("Failed to generate a pixbuf");
            symbolic_img.set_from_pixbuf(Some(&pixbuf));
        }
        Ok(())
    }

    fn init(&self, s: Rc<Self>) {
        let icon_id_entry: gtk::Entry = self
            .builder
            .get_object("icon_id_entry")
            .expect("Failed to get icon_id_entry");
        let rect_id_entry: gtk::Entry = self
            .builder
            .get_object("rect_id_entry")
            .expect("Failed to get rect_id_entry");
        let window = s.clone();
        let weak_icon_id = icon_id_entry.downgrade();
        let weak_rect_id = rect_id_entry.downgrade();
        let rerender = move |_: &gtk::Entry| {
            let icon_id_entry = weak_icon_id.upgrade().unwrap();
            let rect_id_entry = weak_rect_id.upgrade().unwrap();

            let current_id = icon_id_entry.get_text().unwrap();
            let rect_id = rect_id_entry.get_text().unwrap();

            let valid_current_id = current_id != "" && current_id.len() > 2;
            let valid_rect_id = rect_id != "" && rect_id.len() > 2;

            if valid_rect_id && valid_current_id {
                window.rerender();
            }
        };

        icon_id_entry.connect_changed(rerender.clone());
        rect_id_entry.connect_changed(rerender);

        let open_btn: gtk::Button = self
            .builder
            .get_object("open_btn")
            .expect("Failed to get open_btn");
        let window = s.clone();
        open_btn.connect_clicked(move |_| {
            let open_dialog = gtk::FileChooserDialog::with_buttons(
                Some("Open File"),
                Some(&window.widget),
                gtk::FileChooserAction::Open,
                &[
                    ("_Cancel", gtk::ResponseType::Cancel),
                    ("_Open", gtk::ResponseType::Accept),
                ],
            );

            let svg_filter = gtk::FileFilter::new();
            svg_filter.set_name(Some("SVG images"));
            svg_filter.add_mime_type("image/svg+xml");

            open_dialog.add_filter(&svg_filter);

            if gtk::ResponseType::from(open_dialog.run()) == gtk::ResponseType::Accept {
                if let Some(file) = open_dialog.get_file() {
                    window.open_file.replace(Some(file));
                }
            };
            open_dialog.destroy();
        });
    }
}
